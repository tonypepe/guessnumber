package com.tonypepe.guessnumber

import org.junit.Assert.*
import org.junit.Test

class NumberTest {
    @Test
    fun numberTestTenThousandTimes() {
        for (i in 1..10_000) {
            val number = Number()
            assertTrue(number.n in 1..99)
        }
    }

    @Test
    fun guessTrueTestTenThousandTimes() {
        for (i in 1..10_000) {
            val number = Number()
            assertTrue(number.guess(number.n))
        }
    }
}