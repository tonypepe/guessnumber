package com.tonypepe.guessnumber

import android.app.Activity
import android.util.Log

fun Activity.logd(log: String) {
    Log.d(this::class.java.simpleName, log)
}