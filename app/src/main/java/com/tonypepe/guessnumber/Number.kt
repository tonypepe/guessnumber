package com.tonypepe.guessnumber

import java.util.*

class Number {
    var n = Random().nextInt(99) + 1
    var top = 100
    var bottom = 0
    var time = 0

    fun guess(i: Int): Boolean {
        time++
        when {
            i > n -> top = i
            i < n -> bottom = i
            else -> return true
        }
        return false
    }

    fun reset() {
        n = Random().nextInt(100) + 1
        time = 0
        top = 100
        bottom = 0
    }
}