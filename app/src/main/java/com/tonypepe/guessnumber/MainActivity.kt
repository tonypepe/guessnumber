package com.tonypepe.guessnumber

import android.animation.ValueAnimator
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.animation.DecelerateInterpolator
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.jetbrains.anko.alert
import java.util.*
import kotlin.math.absoluteValue

class MainActivity : AppCompatActivity() {
    val number = Number()
    private lateinit var interstitialAd: InterstitialAd

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        setFeb()
        setEditText()
        setAd()
        bt_guess.setOnClickListener {
            if (ed_guess.text.toString().toIntOrNull() in number.bottom..number.top) {
                if (number.guess(ed_guess.text.toString().toInt())) {
                    alert {
                        title = getString(R.string.you_are_right)
                        message = getString(R.string.you_only_need) + " ${number.time} " + getString(R.string.times)
                        positiveButton("Play Again") {
                            replay()
                        }
                        show()
                    }
                } else {
                    setRangeText()
                }
            } else {
                ed_guess.error = getString(R.string.not_in_range)
                ed_guess.text.clear()
            }
        }
        ed_guess.setOnEditorActionListener { v, actionId, event ->
            logd("OnEditorAction")
            bt_guess.performClick()
            return@setOnEditorActionListener false
        }
    }

    private fun setAd() {
        MobileAds.initialize(this, getString(R.string.admob_app_id))
        val adRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
        interstitialAd = InterstitialAd(this).apply {
            adUnitId = "ca-app-pub-1667006488909532/3826682020"
            loadAd(adRequest)
            adListener = object : AdListener() {
                override fun onAdClosed() {
                    super.onAdClosed()
                    loadAd(AdRequest.Builder().build())
                    logd("onAdClosed: reload ad")
                }

                override fun onAdLoaded() {
                    super.onAdLoaded()
                    logd("onAdLoaded: Ad loaded")
                }
            }
        }
    }

    private fun setEditText() {
        ed_guess.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().toIntOrNull() in number.bottom..number.top) {
                    bt_guess.isEnabled = true
                } else {
                    if (s.toString() != "") {
                        ed_guess.error = getString(R.string.not_in_range)
                    }
                    bt_guess.isEnabled = false
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
        ed_guess.setOnEditorActionListener { v, actionId, event ->
            bt_guess.performClick()
            return@setOnEditorActionListener false
        }
    }

    private fun setFeb() {
        fab.setOnClickListener { view ->
            alert {
                title = getString(R.string.replay)
                message = getString(R.string.are_you_sure_to_replay)
                positiveButton(getString(R.string.ok)) {
                    replay()
                }
                negativeButton(getString(R.string.no)) {}
                show()
            }
        }
    }

    private fun setRangeText() {
        ed_guess.text.clear()
        ValueAnimator.ofInt(tx_top.text.toString().toInt(), number.top).apply {
            val d = (35 * (tx_top.text.toString().toInt() - number.top).absoluteValue).toLong()
            duration = if (d > 2000) 2000 else if (d < 300) 300 else d
            interpolator = DecelerateInterpolator()
            addUpdateListener {
                tx_top.text = it.animatedValue.toString()
            }
            start()
        }
        ValueAnimator.ofInt(tx_bottom.text.toString().toInt(), number.bottom).apply {
            val d = (35 * (tx_bottom.text.toString().toInt() - number.bottom).absoluteValue).toLong()
            duration = if (d > 2000) 2000 else if (d < 300) 300 else d
            interpolator = DecelerateInterpolator()
            addUpdateListener {
                tx_bottom.text = it.animatedValue.toString()
            }
            start()
        }
    }

    private fun replay() {
        if (interstitialAd.isLoaded) {
            if (Random().nextInt(100) < 40) {
                interstitialAd.show()
            }
        } else {
            logd("replay: interstitialAd NOT loaded.")
            if (!interstitialAd.isLoading) interstitialAd.loadAd(AdRequest.Builder().build())
        }
        number.reset()
        setRangeText()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
