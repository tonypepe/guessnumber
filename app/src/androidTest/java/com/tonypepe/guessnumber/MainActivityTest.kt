package com.tonypepe.guessnumber

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*

@RunWith(AndroidJUnit4::class)
class
MainActivityTest {
    @get:Rule
    val mainActivityRule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun guessFiveTimes() {
        val activity = mainActivityRule.activity
        val number = activity.number
        for (x in 1..5) {
            var i = 50
            do {
                onView(withId(R.id.ed_guess)).perform(typeText(i.toString()))
                onView(withId(R.id.bt_guess)).perform(click())
                i = Random().nextInt(number.top - number.bottom - 1) + number.bottom + 1
            } while (i != number.n)
            onView(withId(R.id.ed_guess)).perform(typeText(number.n.toString()))
            onView(withId(R.id.bt_guess)).perform(click())
            val message =
                "${activity.getString(R.string.you_only_need)} ${number.time} ${activity.getString(R.string.times)}"
            onView(withText(message)).check(matches(isDisplayed()))
            onView(withText(R.string.play_again)).perform(click())
            try {
                onView(withContentDescription("Interstitial close button")).check(matches(isEnabled()))
                onView(isRoot()).perform(pressBack())
            } catch (e: Exception) {
            }
        }
    }
}